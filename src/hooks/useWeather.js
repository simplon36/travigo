import React, { useEffect, useState } from 'react';

const api = {
    key: "3899ad394074f32464658195fba9ac4e",
    baseUrl: "https://api.openweathermap.org/data/2.5/weather?q=",
    paramsUrl: "&units=metric&lang=fr"
}

function useWeather(props) {
    let cityName = props.replace("-", "%20");
    const [query, setQuery] = useState(cityName);
    const [weatherResult, setWeatherResult] = useState([]);
   
        useEffect(() => {
            fetch (`${api.baseUrl}${query}&appid=${api.key}${api.paramsUrl}`)
            .then(response => response.json())
            .then((result) => {
                const { main, name, sys, weather } = result;
                    setWeatherResult({temp : main.temp});   
                // console.log(name,
                //         sys.country,
                //         main.temp,
                //         weather[0]);
            })
            .then(json => setQuery(json))
            .catch(error => console.error(error));
    }, [])
    return (
        <React.Fragment>{Math.round(weatherResult.temp)}°C</React.Fragment>
    )
}
export default useWeather
