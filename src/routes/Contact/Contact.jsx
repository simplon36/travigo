import ContactForm from "../../components/ContactForm";
import "../../css/contact&newsletter.css";

function Contact() {
    return(
      <div>
        <ContactForm/>
      </div>
    );
  }
  
  export default Contact;