import '../../css/error404.css';

function Error404(){
  return(
    <div id="errorDiv">
      <h2>ERROR 404</h2>
      <h1>Oups, cette page n'existe pas.</h1>
    </div>
    )
}
export default Error404
