import { Link, useParams } from "react-router-dom";
import data from "../../data/data.json";
import useWeather from "../../hooks/useWeather";
import { useNavigate } from "react-router-dom";
import '../../css/destination.css';

function Destination() {

    const navigate = useNavigate();
    const params = useParams();
    const titleOfTheCity =(params['destination']);
    const weatherAPI = useWeather(titleOfTheCity);
    const city = data[titleOfTheCity.toLowerCase()];
    return (<div>
        { !city ? <div class="destinationComponent">

                <div class="heroBannerDestination">
                </div>
                <h2 class="errorMessageDestination">Ce voyage n'existe pas.</h2>
            </div> :
            <div class="destinationComponent">

                <div class="heroBannerDestination">
                    <h1 class="titleDestination">{city['name']}</h1>
                </div>
                <h2>Prêt à embarquer pour {city['name']} ?</h2>
                <div class="divInfosDestination">
                    <div>
                        <p>Coût du voyage : {city['price']}</p>
                        <p>Temps de voyage : {city['time']}</p>
                    </div>
                    <div>
                        <p>Température à {city['name']} : {weatherAPI}</p>
                        <p>Notation : {city['notation']}</p>
                    </div>
                </div>
                <p class="textDestination">{city['description']}</p>
            </div>
            }

       </div> );
  }
  
  export default Destination;
