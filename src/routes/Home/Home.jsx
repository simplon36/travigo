import Banner from "../../components/Banner";
import NewsletterSection from "../../components/NewsletterSection";
import '../../css/home.css';

function Home() {
    return (
    <div>
      <Banner/>
      <NewsletterSection/>
    </div>
    );
  }
  
  export default Home;

