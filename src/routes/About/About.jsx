import Banner from "../../components/Banner";
import WhoWeAre1 from "../../components/WhoWeAre1";
import WhoWeAre2 from "../../components/WhoWeAre2";
import NewsletterSection from "../../components/NewsletterSection";
import "../../css/about.css";


function About() {
    return( 
      <div>
        <Banner/>
        <WhoWeAre1/>
        <WhoWeAre2/>
        <NewsletterSection/>
      </div>
    );
  }
  
  export default About;
