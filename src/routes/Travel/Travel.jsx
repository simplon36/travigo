import CardTravel from "../../components/CardTravel";
import { useState, useEffect } from 'react';
import data from "../../data/data.json";
import '../../css/travel.css';



function Travel() {
  const [search, setSearch] = useState('');
  const [cards, setCards] = useState([]);


  const handleChange = (event)=>{
    setSearch(event.target.value)
  }

  useEffect(()=>{
    setCards([]);
    let tabSearch = [];
    for (const [key, value] of Object.entries(data)) {
       if(key.includes(search.toLowerCase())){
            tabSearch.push(data[key]);
         }
    }
    setCards(tabSearch)
  },[search]);


    return (
        <div class="travelPage">
          <div class="heroBannerTravel">
            <h1 id="titleBannerTravel">Choisissez votre voyage !</h1>
            <input id="searchBar" type="text" value={search} onChange={handleChange} length="50" placeholder="Rechercher votre voyage 🔍"/>
          </div>
            <div id="cards">
                { cards.length!=0 ?  cards.map((city) =>(
                    <CardTravel
                    key={Math.random()}
                    image={city['name']}
                    title={city['name']}
                    price={city['price']}
                    time={city['time']}
                    notation={city['notation']}/>)) : <p id="nullSearch">Aucun voyage ne correspond à votre recherche.</p> }
            </div>
        </div>
        )
  }
  
  export default Travel;
