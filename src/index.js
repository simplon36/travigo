import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import reportWebVitals from './reportWebVitals';
import { BrowserRouter as Router } from "react-router-dom";
import { Routes, Route as ReactRoute } from "react-router";

import Home from "./routes/Home";
import About from "./routes/About";
import Travel from "./routes/Travel";
import Destination from "./routes/Destination";
import Contact from "./routes/Contact";
import Header from "./components/Header";
import Footer from "./components/Footer";
import Error404 from "./routes/Error404";
import './css/index.css';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <Router>
     <Header/>
      <Routes>
        <ReactRoute path="/" element={<Home/>}></ReactRoute>
        <ReactRoute path="/a-propos" element={<About/>}></ReactRoute>
        <ReactRoute path="/voyages" element={<Travel/>}></ReactRoute>
        <ReactRoute path="/voyages/:destination" element={<Destination/>}></ReactRoute>
        <ReactRoute path="/Contact" element={<Contact/>}></ReactRoute>
        <ReactRoute path="*" element={<Error404/>}></ReactRoute>
      </Routes>
      <Footer/>
    </Router>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
