import '../../css/banner.css';

function Banner() {
    return(
        <div class="divBanner">
            <h1 id="titleBanner">Travigo
                <br/>
                Voyage
            </h1>
            <p id="textBannerHome">
            Explorez nos voyages et vivez The Good Life avec Travigo
            <br/>
            Tours qui vous font tomber amoureux du monde.
            </p>
            <input id="buttonBanner" type="button" value="Explorez nos voyages" />
        </div>
    );
}

export default Banner;
