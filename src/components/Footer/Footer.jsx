import {Link} from "react-router-dom";
import QuickLinks from "./footerComponents/QuicksLinks";
import Contact from "./footerComponents/Contact";
import SocialMedia from "./footerComponents/SocialMedia";
import '../Footer/footer.css';


function Footer(){
  return(
    <div class="footer">
      <div class="footerLink">
        <QuickLinks/>
        <Contact/>
        <SocialMedia/>
      </div>
        <p id="copyright" >Copyright ©2022 Tout droits réserver | Travigo</p>
    </div>
    )
}
export default Footer
