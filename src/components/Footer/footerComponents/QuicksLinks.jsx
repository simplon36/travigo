import {Link} from "react-router-dom";

function QuickLinks(){

  return(
      <div class="quickLinks">
          <h2>Liens rapides</h2>
          <ul>
            <li>
              <Link class="linkQuickLinks" to="/a-propos">A Propos</Link>
            </li>
            <li>
              <Link class="linkQuickLinks" to="/mentions-legal">Mentions Légales</Link>
            </li>
            <li>
              <Link class="linkQuickLinks" to="/politique-de-confidentialités"> Politique de confidentialités</Link>
            </li>
          </ul>
        </div>
    )
}
export default QuickLinks
