function WhoWeAre1() {
    return(
        <div class="allabout">
            <div class="about">
            <h2>Qui sommes nous</h2>
            <p>
            Fondé en 2005 par trois amis universitaires à Düsseldorf (Allemagne), 
            trivago est depuis devenu l'un des principaux sites mondiaux de recherche 
            d'hébergements. Nous nous concentrons sur la refonte de la façon dont 
            des millions de voyageurs recherchent et comparent les hôtels et autres 
            hébergements. Membre du groupe Expedia (NASDAQ : TRVG), la mission de trivago 
            est de devenir votre compagnon pour découvrir notre monde.
            </p>
            </div>
            <div class="boat">
            <img src="../img/boat.jpg" alt="boat"/>
            </div>
        </div>
    );
}

export default WhoWeAre1;