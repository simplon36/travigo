import React from "react";
import {Link} from "react-router-dom";
import '../../css/header.css';

function Header(){


const menu = (event=>{
  let navForMobile = document.querySelector('.navForMobile');
  let logo = document.getElementById('logoImg');
  if(window.screen.width<700 && navForMobile.style.display == 'none'){
    navForMobile.style.display = 'flex';
    logo.src = '../img/logo2.png';
  }else{
    navForMobile.style.display = 'none';
    logo.src = '../img/logo.png';
  }
})


  window.addEventListener('resize',e=>{
    let navForMobile = document.querySelector('.navForMobile');
    let logo = document.getElementById('logoImg');
    if(window.screen.width>700){
       navForMobile.style.display = 'none';
       logo.src = '../img/logo.png';

    }
  })


  window.addEventListener('scroll',e=>{

    let divHeader = document.querySelector('.header');
    let links = document.querySelectorAll('.link');
    let logo = document.getElementById('logoImg');

    if(window.scrollY!=0){
      divHeader.style.backgroundColor ='white';
      logo.src = '../img/logo2.png';
      links.forEach(link=>{
        link.style.color ='black';
      })
    }else{
      divHeader.style.backgroundColor ='transparent';
      logo.src = '../img/logo.png';
      links.forEach(link=>{
        link.style.color ='white';
      })
    }
  })

  return(
    <div>
    <div class="header">
      <Link id="logo" onClick={menu}><img id="logoImg"src="../img/logo.png" alt="LOGO"/></Link>
      <nav>
        <ul>
          <li>
            <Link class="link" to="/">Accueil</Link>
          </li>
          <li>
            <Link class="link" to="/a-propos">A-propos</Link>
          </li>
          <li>
            <Link class="link" to="/voyages">Voyages</Link>
          </li>
          <li>
            <Link class="link" to="/contact">Contact</Link>
          </li>
        </ul>
      </nav>

    </div>
    <nav class="navForMobile">
        <ul>
          <li>
            <Link class="linkMobile" to="/">Accueil</Link>
          </li>
          <li>
            <Link class="linkMobile" to="/a-propos">A-propos</Link>
          </li>
          <li>
            <Link class="linkMobile" to="/voyages">Voyages</Link>
          </li>
          <li>
            <Link class="linkMobile" to="/contact">Contact</Link>
          </li>
        </ul>
      </nav>
    </div>
    )
}

export default Header;
