function WhoWeAre2() {
    return(
        <div class="allabout">
            <div class="maroc">
            <img class="maroc-pic" src="../img/maroc.jpg" alt="maroc"/>
            </div>
            <div class="about">
            <p>
            Nous organisons des voyages parfaits partout au Maroc. 
            Nous voulons que vous appréciiez et soyez ému par votre expérience ici, 
            que ce soit votre première fois ou un voyage de retour. 
            Nous voulons que vous voyiez notre beau pays comme vous le souhaitez. 
            Nous concevrons un voyage qui convient à votre façon de voyager et 
            à ce que vous appréciez. Envie de Luxe ? Aventure? Simplicité? 
            Il y a tellement de façons de concentrer votre temps ici, 
            choisissons les bonnes choses pour vous. Tout au long, 
            OmegaTours s'occupe de tout, du jour où vous atterrissez jusqu'au moment 
            où nous disons au revoir à l'aéroport. 
            Nous nous occupons de toute la logistique : planification des voyages dans le pays ;
             réserver un logement ; et fournir un transport confortable et privé. 
             Mais la chose la plus importante, ce sont nos gens : les guides et les chauffeurs 
             compétents et sympathiques d'Omega connaissent et aiment cette terre, et aiment la partager avec les visiteurs. 
             Nous avons également un merveilleux réseau de personnes à héberger et à vous aider à approfondir ce que vous souhaitez voir : 
             la liberté d'explorer en équilibre avec votre sécurité. Venez découvrir le Maroc avec nous ! Vous ne serez pas déçu.
            </p>
            </div>
        </div>
    );
}

export default WhoWeAre2;