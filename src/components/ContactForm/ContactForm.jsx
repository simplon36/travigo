function ContactForm() {
    return(
        <div>
        <div class="banner-contact">
            <h1>Contactez Nous !</h1>
        </div>
        <div class="form">
            <form action="POST" >
                <input class="name" type="name" name="name" placeholder="Votre nom et prénom" required/>
                <br/>
                <input class="email" type="email" name="email" placeholder="Votre email" required/>
                <br/>
                <textarea class="text" name="text" placeholder="Votre message" required></textarea>
                <br/>
                <input class="submit" type="submit" value="Envoyer" />
            </form>
        </div>
        </div>
    );
}

export default ContactForm;