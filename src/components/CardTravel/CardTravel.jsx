import React from "react";
import { useNavigate } from "react-router-dom";
import  useWeather from "../../hooks/useWeather";
import '../CardTravel/cardTravel.css';

function CardTravel ({image,title,price,time,notation}){
  const weatherAPI = useWeather(title);
  const navigate = useNavigate();

  const redirection = (event)=>{
    navigate('/voyages/'+title.toLowerCase());
  };
    return(
      <div class="card" onClick={redirection}>
        <img class="cardImg" src={"/img/"+image+".jpg"} alt=""/>
        <div id="firstInfoCard">
          <h2 id="titleCard">{title.toUpperCase()}</h2>
          <p id="notationCard">{notation}</p>
        </div>
        <div id="secondInfoCard">
            <p id="infoCard">{price} • {weatherAPI} • {time}</p>
        </div>
      </div>

      )
}
export default CardTravel
