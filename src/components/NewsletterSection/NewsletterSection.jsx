import NewsletterForm from "./NewsletterForm/NewsletterForm";

function NewsletterSection() {
    return(
        <div class="newsletter">
            <h2>Newsletter</h2>
            <p>Abonnez-vous pour recevoir les meilleures offres !
            </p>
            <NewsletterForm/>
        </div>
    );
}

export default NewsletterSection;